# Crie um programa que leia duas notas de um aluno e calcule sua média, mostrando sua mona no final, de acordo com a média atingida:

# - Média abaixo de 5.0: REPROVADO;
# - Média entre 5.0 a 6.9: RECUPERAÇÃO;
# - Média 7.0 ou superior: APROVADO.

grade1 = float(input("digite o valor da primeira nota: "))
grade2 = float(input("digite o valor da segunda nota: "))
average = (grade1 + grade2) / 2

if average < 5.0: 
	print("Você foi REPROVADO!")

if average >= 5.0 and average <=6.9:
	print("Você ficou em RECUPERAÇÃO!")

if average >= 7.0:
	print("Você foi APROVADO!")

print(f"Sua média foi de {average:.2f}")
