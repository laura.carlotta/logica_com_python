( () => {

    const buttonSendInfos = document.querySelector( '[data-send-button]' );

    buttonSendInfos.addEventListener( 'click', function () {
        const divResult = document.querySelector( '[data-result]' );
        let message = '';
        let grades = document.querySelectorAll( '.grades' );
        let gradesValue = [];

        const showMessage = ( classList, message ) => {
            divResult.textContent = message;
            $( '#div-result' ).removeClass();
            divResult.classList.add( classList );
        }

        showMessage( 'none', '' );

        const validInputs = () => {
            for ( let i = 0; i < grades.length; i++ ) {
                if ( !grades[ i ].value || isNaN( grades[ i ].value ) || grades[ i ].value < 0 ) {
                    message = 'Informe um valor válido para os dois campos!';
                    showMessage( 'error', message );
                }

                if ( grades[ i ].value > 0 ) {
                    console.log( "Tem valor aqui" );
                    gradesValue.push( Number( grades[ i ].value ) );
                    let addedValues = gradesValue.reduce( function ( addedValues, i ) {
                        return ( addedValues + i ) / gradesValue.length;
                    } );

                    if ( addedValues < 5.0 ) {
                        message = `Sua nota foi ${ addedValues }...`;
                        message += 'VOCÊ FOI REPROVADO!'
                        showMessage( 'disaprove', message )
                    }

                    if ( addedValues >= 5.0 && addedValues <= 6.9) {
                        message = `Sua nota foi ${ addedValues }...`;
                        message += 'VOCÊ FICOU EM RECUPERAÇÃO!'
                        showMessage( 'recuperation', message )
                    }
                    
                    if ( addedValues > 7.0 ) {
                        message = `Sua nota foi ${ addedValues }...`;
                        message += 'VOCÊ FOI APROVADO!'
                        showMessage( 'approve', message )
                    }
                }
            }
        }

        validInputs()

        /* const validInputs = ( grade ) => {

            if ( grade.value == 0 ) {
                console.log( 'falta esse input', grade );

                message = 'Preencha os dois campos!';
                showMessage( 'error', message );
                console.log( divResult );


            }

            if ( grade.value > 0 ) {

                gradesValue += gradesValue.push( Number( grade.value ) );
                console.log( typeof gradesValue );
                console.log( gradesValue );
            }
        } */

    } );

} )();