# Escreva um programa que leia um numero inteiro qualquer e peça para o usuário escolher qual será a base de conversão:

# 1 - para binário
# 2 - para octal
# 3 - para hexadecimal

number = int(input('Informe um número: '))
option = int(input('Você quer convertê-lo para: \n1-Binário; \n2-Octal; \n3-Hexadecimal: \n'))
conversion = 0
    

if option == 1:
    conversion = bin(number)[2:]
    print(f'binario: {conversion}')

elif option == 2:
    conversion = oct(number)[2:]
    print(f'octal: {conversion}')
    
elif option == 3:
    conversion = hex(number)[2:]
    print(f'hexadecimal: {conversion}')

else:
    print('Digite uma opção válida!')
