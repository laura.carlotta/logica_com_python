houseValue = float(input("Informe o valor da casa: R$ "))
salaryValue = float(input("Informe o valor do seu salário: R$ "))
howManyYearsYouWillPayThisHouse = int(input("Em quantos anos você irá pagar a casa? "))
partValue = (houseValue / (howManyYearsYouWillPayThisHouse * 12))
thirtyPercentoftheSalary = salaryValue * 30 /100

print("")
print(f"Para comprar uma casa de R$ {houseValue:.2f} reais em {howManyYearsYouWillPayThisHouse} anos, \nCada parcela será de R$ {partValue:.2f} reais.")

if partValue >=  thirtyPercentoftheSalary: 
    print(f"EMPRÉSTIMO NEGADO! \n30% do seu salário é de R$ {thirtyPercentoftheSalary:.2f}reais. \nO valor de cada parcela ultrapassa 30% do seu salário!")
    
else:
    print("EMPRÉSTIMO APROVADO!")
