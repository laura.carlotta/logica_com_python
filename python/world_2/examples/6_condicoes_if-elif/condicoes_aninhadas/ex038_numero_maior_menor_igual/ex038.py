# Escreva um programa que leia dois numeros inteiros e compare-os, mostrando na tela uma mensagem:

# - O primeiro valor é maior
# - O segundo valor é maior
# - Não existe valor maior, os dois são iguais

number1 = int(input('Digite o primeiro número: '))
number2 = int(input('Digite o segundo número: '))

if number1 > number2:
	print(f'O primeiro valor é maior!')

elif number1 < number2:
	print(f'O segundo valor é maior!')

else:
	print(f'Não existe valor maior, os dois são iguais!')

print(f'Obrigada por usar o nosso programa!')
