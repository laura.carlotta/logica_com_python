( () => {

    const buttonSendInfos = document.querySelector( '[data-send-button]' );

    buttonSendInfos.addEventListener( 'click', function () {

        const number1Input = document.querySelector( '[data-number=number1]' ).value;
        const number2Input = document.querySelector( '[data-number=number2]' ).value;
        const number1 = Number( document.querySelector( '[data-number=number1]' ).value );
        const number2 = Number( document.querySelector( '[data-number=number2]' ).value );
        const divResult = document.querySelector( '[data-result]' );
        let message = '';

        const validInputs = ( number1, number2 ) => {
            if ( number1.length == 0 || number2.length == 0 ) {
                message = 'Os campos não podem estar vazios';
                errorMessage( divResult, message, 'error' );
                return true;
            }
        }

        const errorMessage = ( divMessage, message, addClass ) => {
            divMessage.textContent = message;
            $( '#div-result' ).removeClass();
            divMessage.classList.add( addClass );
        }

        if ( !validInputs( number1Input, number2Input ) ) {

            errorMessage(divResult, '', 'none')
            if ( number1 > number2 ) {
                message = 'O primeiro número é maior que o segundo número!';
                errorMessage( divResult, message, 'true-yellow' );
            }

            if ( number1 < number2 ) {
                message = 'O primeiro número é menor que o segundo número!';
                errorMessage( divResult, message, 'true-green' );
            }

            if ( number1 == number2 ) {
                message = 'O primeiro número é igual ao segundo número!';
                errorMessage( divResult, message, 'true-blue' );
            }
        }

    } );

} )();

