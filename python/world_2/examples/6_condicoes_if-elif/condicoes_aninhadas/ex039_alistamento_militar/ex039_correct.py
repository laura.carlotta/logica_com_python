from datetime import date

atualYear = date.today().year
birthYear = int(input("Qual o seu ano de nascimento: "))
typeOfSex = int(input("""Qual o seu sexo:
                      [1] FEMININO
                      [2] MASCULINO """))
age = atualYear - birthYear
howManyYears = 0
alistamentYear = 0

print("")
print(f"Quem nasceu em { birthYear } tem { age } anos em { atualYear }.")

if typeOfSex == 1: 
    print("O alistamento obrigatório é somente para pessoas do sexo masculino. \nVocê não precisa se alistar.")
    
elif typeOfSex == 2:

    if age == 18: 
        print("Você presisa se alistar IMEDIATAMENTE!!")
        
    elif age < 18: 
        howManyYears = 18 - age
        alistamentYear = atualYear + howManyYears
        print(f"Você ainda não tem 18 anos. Faltam { howManyYears } anos para o seu alistamento que será em { alistamentYear }.")
        
    elif age > 18:
        howManyYears = age - 18
        alistamentYear = atualYear - howManyYears
        print(f"Você já passou dos 18 anos. Você está atrasado { howManyYears } anos. Você deveria ter se alistado em { alistamentYear }. ")
    
else:
    print("Informe uma opção válida! ")
