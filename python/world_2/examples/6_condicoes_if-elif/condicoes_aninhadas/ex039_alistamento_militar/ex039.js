( () => {

    const buttonSendInfos = document.querySelector( '[data-send-button]' );

    buttonSendInfos.addEventListener( 'click', function () {

        const birthdate = document.querySelector( '[data-birthdate]' );
        let divResult = document.querySelector( '[data-result]' );
        const today = new Date();
        let age = '';
        let message = '';

        /* User Date*/
        birthdateUser = String( birthdate.value )
        birthdateUserSlice = birthdateUser.split( "-" )
        dayUser = Number( birthdateUserSlice[ 2 ] );
        monthUser = Number( birthdateUserSlice[ 1 ] );
        yearUser = Number( birthdateUserSlice[ 0 ] );

        // console.log('de cima', Number(age));

        const validityAge = () => {
            age = today.getFullYear() - yearUser;

            if ( today.getMonth() + 1 != monthUser ) {

                if ( today.getMonth() + 1 < monthUser ) {
                    age--
                }
            } else {
                if ( today.getDate() < dayUser ) {
                    age--
                }
            }

            return age;
        }

        // console.log('debaixo', Number(age));

        const validInputs = (input) => {
            if (input.value == 0 ) {
                message = 'Informe sua data de nascimento!';
                showMessage( 'error', message );
                return true;                             
            }
        }

        const showMessage = ( classList, message ) => {
            divResult.innerHTML = message;
            $( '#div-result' ).removeClass();
            divResult.classList.add( classList );
        }

        
        if (!validInputs(birthdate)) {

            showMessage('none', '');

            if ( validityAge() < 17 ) {
                message = ` <em>Você tem ${ age } anos!</em> <br> 
                            Você ainda não tem idade para se alistar no exército!`
                showMessage( 'blue-color', message );
            }
    
            if ( validityAge() == 17 ) {
                message = 'Você já deve se alistar no exército';
                showMessage( 'true', message );
            }
    
            if ( validityAge() == 18 ) {
                message = 'Você já deve estar no exército!';
                showMessage( 'true', message );
            }

            if ( validityAge() > 18 ) {
                let except = age - 18;
                message = `Você já está atrasado ${ except } anos para o alistamento militar`;
                showMessage( 'error', message );
            }
        }
        

    } );

} )();