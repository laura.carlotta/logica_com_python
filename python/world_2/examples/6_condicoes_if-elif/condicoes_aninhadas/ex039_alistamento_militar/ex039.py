# Faça um programa que leia o ano de nascimento de um jovem e informe, de acordo com a sua idade:

# - Se ele ainda vai se alistar ao serviço militar
# - Se é hora de se alistar
# - Se já passou do tempo de alistamento

# Seu programa também deverá mostrar o tempo que falta ou o tempo que passou do prazo.

bithdate = int(input('Em que ano você nasceu? '))
atualYear = 2021

result = atualYear - bithdate

if result < 17:
	print(f'Você tem {atualYear - bithdate} anos.')
	print('Você ainda não tem idade de se alistar!')

if result == 17:
	print(f'Você tem {atualYear - bithdate} anos.')
	print('Você precisa se alistar')

if result >=18:
	print(f'Você tem {atualYear - bithdate} anos.')
	print('Já passou do tempo de se alistar')
	
	if result > 18:
		print(f"Já se passaram {result - 18} anos da data certa para o seu alistamento")
