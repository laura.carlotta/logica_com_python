( () => {

    const buttonSendInfos = document.querySelector( '[data-send-button]' );

    buttonSendInfos.addEventListener( 'click', function () {

        const inputsList = document.querySelectorAll( '[data-inputs="input"]' );
        let divMessage = document.querySelector('[data-result]');
        let inputValueListNumbers = [];
        let message = '';
        
        const validInput = ( input, i ) => {

            if (inputValueListNumbers && input.value > 0 && !isNaN(inputValueListNumbers)) {
                inputValueListNumbers.push(Number(input.value));

                console.log(input.value, i);
                console.log(inputValueListNumbers);

                message = "Mostra tudo";
                showMessage('true', message);
            }

            /* if () {
                console.log("Não tem nada");
                message = "Não tem nada";
                showMessage('error', message);
            }  */
            
        }

        const showMessage = (classCSS, message) => {
            $('[data-result]').removeClass();
            divMessage.classList.add(classCSS);
            divMessage.textContent = message;
        }

        inputsList.forEach( validInput );

    } );

} )();