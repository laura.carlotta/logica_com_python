# Elabore um programa que calcule o valor a ser pago por um produto, considerando o seu preço normal e a condição de pagamento:

# - à vista dinheiro/cheque: 10% DE DESCONTO;
# - à vista no cartão: 5% DE DESCONTO;
# - em até 2x no cartão: PREÇO NORMAL;
# - 3x ou mais no cartão: 20% DE JUROS. ==>> acrescentar valores de juros a cada parcela.

print("")
product = float(input("Insira o valor do produto: R$ "))

print("")
print(f"Conforme o valor informado, R$ {product:.2f}, escolha uma das formas de pagamento: ")
print("")
print("1 - à vista dinheiro/cheque: 10% DE DESCONTO;")
print("2 - à vista no cartão: 5% DE DESCONTO;")
print("3 - em até 2x no cartão: PREÇO NORMAL;")
print("4 - 3x ou mais no cartão: 20% DE JURTOS.")
print("")

option = int(input("Digite uma das opções: "))
print("")

finalValue = 0

if option == 1:
	finalValue = product - (product * 10 / 100)
	print(f"Para forma de pagamento à vista em dinheiro ou cheque você terá 10% de desconto, \nVocê irá pagar: R$ {finalValue:.2f} reias.")

elif option == 2:
	finalValue = product - (product * 5 / 100)
	print(f"Para forma de pagamento à vista no cartão você terá 5% de desconto, \nVocê irá pagar: R$ {finalValue:.2f} reias.")

elif option == 3: 
	finalValue = product / 2
	print(f"Para forma de pagamento em até 2x no cartão, o valor não terá juros, \nVocê irá pagar: R$ {product:.2f} reias, sendo aproximadamente R$ {finalValue:.2f} reais por parcela.")

elif option == 4:

	valuePerParts = 0
	totalValueWithFees = 0
	howMuchParts = int(input("Em quantas parcelas você vai quer pagar? (Lembrando que o valor máximo de parcelas é de até 12x): "))

	if howMuchParts == 1: 
		finalValue = product - (product * 5 / 100)
		print(f"Para forma de pagamento à vista no cartão você terá 5% de desconto, \nVocê irá pagar: R$ {finalValue:.2f} reias.")

	elif howMuchParts == 2: 
		finalValue = product / 2
		print(f"Para forma de pagamento em até 2x no cartão, o valor não terá juros, \nVocê irá pagar: R$ {product:.2f} reias, sendo aproximadamente R$ {finalValue:.2f} reais por parcela.")

	elif howMuchParts >=3 and howMuchParts <= 12:
		valuePerParts = (product / howMuchParts) + (product * 20 / 100)
		totalValueWithFees = valuePerParts * howMuchParts
		print(f"Para forma de pagamento em {howMuchParts} parcelas, cada uma das parcelas terá o acréssimo de 20% do valor total, \nPara o produto com valor de R$ {product:.2f} reais \nVocê irá pagar: R$ {totalValueWithFees:.2f} reias por esse produto, sendo aproximadamente R$ {valuePerParts:.2f} reais por parcela.")

	else:
		print("Informe uma quantidade de parcelas válida!")

else:
	print("Informe uma opção válida!")
