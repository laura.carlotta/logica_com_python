(() => {

		const dataInput = document.querySelector( '[data-input]' );
		const dataButton = document.querySelector( '[data-button]' );
		const dataAnswer = document.querySelector( '[data-answer]' );

		const confirm = () => {
			if ( !validateInput() ) {

				if ( Number( dataInput.value ) % 3 == 0 ) {
						addClassMessage('number-fizz-buzz')
						dataAnswer.textContent = 'Fizz';

						if (Number( dataInput.value ) % 5 == 0) {
								addClassMessage('number-fizz-buzz')
								dataAnswer.textContent = 'Fizzbuzz';
						}
				}

				if ( Number( dataInput.value ) % 5 == 0 ) {
						addClassMessage('number-fizz-buzz')
						dataAnswer.textContent = 'Buzz';

						if (Number( dataInput.value ) % 3 == 0) {
								addClassMessage('number-fizz-buzz')
								dataAnswer.textContent = 'Fizzbuzz';
						}
				}

				if ( Number( dataInput.value ) % 3 != 0 && Number( dataInput.value ) % 5 != 0 ) {
					dataAnswer.textContent = 'Este é um número comum!';
					addClassMessage('comum-number');
				}
			}
		}

		const validateInput = () => {
				dataAnswer.textContent = '';

				if( dataInput.value.length == 0 || isNaN( dataInput.value ) ) {
						dataAnswer.textContent = 'Informe um valor válido!';
						addClassMessage('error-message');

						return true;
				}
		}

		const addClassMessage = ( classCss ) => {
 				$(dataAnswer).removeClass();
				dataAnswer.classList.add( classCss );
		}

		dataButton.addEventListener( 'click', confirm );

}) ();
