const browsersList = [ 'Firefox', 'Opera', 'Chrome', 'Vivaldi', 'Safari', 'Edge', 'IE' ];

console.log('FOR');
console.log('-------------------------------------');
for ( let i = 0; i < browsersList.length; i++ ) {
	console.log( browsersList[ i ], i );
}

console.log('');
console.log('FOREACH');
console.log('-------------------------------------');
browsersList.forEach( function ( browser, i ) {
	console.log( browser, i );
} )


