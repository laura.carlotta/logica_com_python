const browsersList = [ 'firefox', 'opera', 'chrome', 'Vivaldi', 'Safari', 'edge', 'IE' ];

const retornoDoForEach = browsersList.forEach( function ( browser, i ) {
	console.log( browser, i );
} )

console.log(retornoDoForEach);





// deixa eu manipular o retorno!
const retornoDoMap = browsersList.map( function ( browser, i ) {
	console.log( browser, i );

	// return 'aaaa';
	return browser[0].toUpperCase() + browser.slice(1); // capitalize
} )

console.log(retornoDoMap);





const outroRetornoDoMap = browsersList.map( function ( browser, i ) {
	console.log( browser, i );

	// return 'aaaa';
	return /* html */ `<li>${browser.toUpperCase()}</li>`;
} )

console.log(outroRetornoDoMap);
