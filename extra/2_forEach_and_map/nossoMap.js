const browsersList = [ 'Firefox', 'Opera', 'Chrome', 'Vivaldi', 'Safari', 'Edge', 'IE' ];

function nossoMap ( array, funcao ) {

	const novoArray = [];

	for ( let indice = 0; indice < array.length; indice++ ) {
		const itemDoArray = array[ indice ];
		novoArray.push( funcao( itemDoArray, indice ) );
		// funcao( array[indice], indice );
	}

	// return array;
	return novoArray;
}

/* const retornoDoNossoMap = nossoMap( browsersList, function ( browser, i ) {
	console.log( browser, i );
	
	return `<p>Navegador: ${browser}</p>` 
} ) */

const retornoDoNossoMap = browsersList.map( function ( browser, i ) {
	console.log( browser, i );

	return `<p>Navegador: ${ browser }</p>`
} )

console.log( retornoDoNossoMap );
