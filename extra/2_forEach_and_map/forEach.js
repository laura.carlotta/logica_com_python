const browsersList = [ 'Firefox', 'Opera', 'Chrome', 'Vivaldi', 'Safari', 'Edge', 'IE' ];

browsersList.forEach( function ( browser, i ) {
	console.log( browser, i );
} )

function nossoForEach ( array, funcao ) {
	for ( let indice = 0; indice < array.length; indice++ ) {
		const itemDoArray = array[ indice ];
		funcao( itemDoArray, indice );
		// funcao( array[indice], indice );
	}
}

nossoForEach( browsersList, function ( browser, i ) {
	console.log( browser, i );
} )